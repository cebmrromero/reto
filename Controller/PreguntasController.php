<?php
App::uses('AppController', 'Controller');
/**
 * Preguntas Controller
 *
 * @property Pregunta $Pregunta
 * @property PaginatorComponent $Paginator
 */
class PreguntasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * seleccionar method
 *
 * @return void
 */
	public function seleccionar($equipo_id) {
		$this->Pregunta->Torneo->recursive = 0;
		$torneo = $this->Pregunta->Torneo->findByActivo('1');
		if (!$torneo) {
			$this->redirect(array('controller' => 'torneos', 'action' => 'activo'));
		}

		$this->Pregunta->recursive = -1;
		$preguntas = $this->Pregunta->findAllByTorneoIdAndRondaId($torneo['Torneo']['id'], $this->Session->read('ronda_actual'));
		// print_r($preguntas);die;
		$this->set(compact('preguntas', 'equipo_id'));
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$torneo = $this->Pregunta->Torneo->findByActivo('1');
		if ($torneo) {
			$this->Paginator->settings = array(
				'conditions' => array('Pregunta.torneo_id' => $torneo['Torneo']['id']),
				'limit' => 10,
				'order' => 'Pregunta.id DESC'
			);
		}
		$this->Pregunta->recursive = 0;
		$preguntas = $this->Paginator->paginate();
		$this->set(compact('preguntas', 'torneo'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Pregunta->exists($id)) {
			throw new NotFoundException(__('Invalid pregunta'));
		}
		$options = array('conditions' => array('Pregunta.' . $this->Pregunta->primaryKey => $id));
		$this->set('pregunta', $this->Pregunta->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Pregunta->create();
			if ($this->Pregunta->saveAll($this->request->data)) {
				$this->Session->setFlash(__('La pregunta ha sido agregada'), 'flash_success');
				return $this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('La pregunta no se pudo agregar, intente de nuevo'), 'flash_error');
			}
		}
		$torneo = $this->Pregunta->Torneo->findByActivo(1);
		$rondas = $this->Pregunta->Ronda->find('list');
		$respuestas = array('A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D');
		$ultima_pregunta = $this->Pregunta->find('first', array('conditions' => array('Pregunta.torneo_id' => $torneo['Torneo']['id']), 'order' => 'Pregunta.id DESC'));
		$cantidad_eliminatoria = $this->Pregunta->find('count', array('conditions' => array('Pregunta.ronda_id' => 1, 'Pregunta.torneo_id' => $torneo['Torneo']['id'])));
		$cantidad_final = $this->Pregunta->find('count', array('conditions' => array('Pregunta.ronda_id' => 2, 'Pregunta.torneo_id' => $torneo['Torneo']['id'])));
		$this->set(compact('torneo', 'rondas', 'respuestas', 'ultima_pregunta', 'cantidad_eliminatoria', 'cantidad_final'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Pregunta->exists($id)) {
			throw new NotFoundException(__('Invalid pregunta'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Pregunta->save($this->request->data)) {
				$this->Session->setFlash(__('La pregunta ha sido actualizada'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La pregunta no pudo ser actualizada, intente de nuevo'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Pregunta.' . $this->Pregunta->primaryKey => $id));
			$this->request->data = $this->Pregunta->find('first', $options);
		}
		$respuestas = array('A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D');
		$torneos = $this->Pregunta->Torneo->find('list');
		$rondas = $this->Pregunta->Ronda->find('list');
		$this->set(compact('torneos', 'rondas', 'respuestas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Pregunta->id = $id;
		if (!$this->Pregunta->exists()) {
			throw new NotFoundException(__('Invalid pregunta'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Pregunta->delete()) {
			$this->Session->setFlash(__('La pregunta ha sido eliminada'), 'flash_success');
		} else {
			$this->Session->setFlash(__('No se pudo eliminar la pregunta, intente de nuevo'), 'flash_error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
