<?php
App::uses('AppController', 'Controller');
/**
 * Equipos Controller
 *
 * @property Equipo $Equipo
 * @property PaginatorComponent $Paginator
 */
class EquiposController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Equipo->recursive = 0;
		$this->set('equipos', $this->Paginator->paginate());
	}

	public function getEquiposForResumen($finales = false) {
		$this->Equipo->Torneo->recursive = 0;
		$torneo = $this->Equipo->Torneo->findByActivo('1');
		if (!$torneo) {
			return array();
		}
		$ronda = 1;
		if ($finales) {
			$ronda = 2;
		}
		$equipos_torneo = $this->Equipo->getEquiposWithCountByTorneoId($torneo['Torneo']['id'], $this->Session->read('fase_actual'), $ronda);
		foreach ($equipos_torneo as &$equipo) {
			$this->Equipo->Respuesta->recursive = -1;
			$equipo['Respuesta'] = $this->Equipo->Respuesta->findAllByEquipoIdAndRondaId($equipo['Equipo']['id'], $ronda);
		}
		return $equipos_torneo;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Equipo->exists($id)) {
			throw new NotFoundException(__('Invalid equipo'));
		}
		$options = array('conditions' => array('Equipo.' . $this->Equipo->primaryKey => $id));
		$this->set('equipo', $this->Equipo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$torneo = $this->Equipo->Torneo->findByActivo('1');
		if ($this->request->is('post')) {
			$this->Equipo->create();
			// print_r($this->request->data['Equipo']);die;
			$this->Equipo->Participante->recursive = -1;
			foreach ($this->request->data['Equipo'] as &$equipo) {
				$aux = $this->Equipo->Participante->findById($equipo['participante_id']);
				$equipo['nombre'] = $aux['Participante']['nombre_color'];
			}
			if ($this->Equipo->saveMany($this->request->data['Equipo'])) {
				$torneo['Torneo']['iniciado'] = 1;
				$this->Equipo->Torneo->save($torneo); // TODO: Hacer metodo para activar o desactivar torneo en el modelo
				$this->Session->setFlash(__('Los equipos han sido agregados'), 'flash_success');
				return $this->redirect(array('controller' => 'torneos', 'action' => 'activo'));
			} else {
				$this->Session->setFlash(__('No se pudo agregar equipos, intente de nuevo'), 'flash_error');
			}
		}
		$participantes = $this->Equipo->Participante->findAllByTorneoId($torneo['Torneo']['id']);
		$participantes_options = array();
		foreach ($participantes as $participante) {
			$participantes_options[$participante['Participante']['id']] = 'Equipo ' . $participante['Participante']['nombre_color'] . ' - ' . $participante['Participante']['denominacion'];
		}
		$cantidad = sizeof($participantes);
		$this->set(compact('torneo', 'cantidad', 'participantes', 'participantes_options'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Equipo->exists($id)) {
			throw new NotFoundException(__('Invalid equipo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Equipo->save($this->request->data)) {
				$this->Session->setFlash(__('El equipo ha sido actualizado'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El equipo no pudo ser actualizado, intente de nuevo'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Equipo.' . $this->Equipo->primaryKey => $id));
			$this->request->data = $this->Equipo->find('first', $options);
		}
		$torneos = $this->Equipo->Torneo->find('list');
		$rondas = $this->Equipo->Ronda->find('list');
		$this->set(compact('torneos', 'rondas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Equipo->id = $id;
		if (!$this->Equipo->exists()) {
			throw new NotFoundException(__('Invalid equipo'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Equipo->delete()) {
			$this->Session->setFlash(__('El equipo ha sido eliminado'), 'flash_success');
		} else {
			$this->Session->setFlash(__('El equipo no pudo ser eliminado, intente de nuevo'), 'flash_error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
