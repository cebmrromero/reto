<?php
App::uses('AppController', 'Controller');
App::uses('String', 'Utility');
/**
 * Torneos Controller
 *
 * @property Torneo $Torneo
 * @property PaginatorComponent $Paginator
 */
class TorneosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * activo method
 *
 * @return void
 */
	public function activo() {
		$this->Torneo->recursive = 1;
		$torneo = $this->Torneo->findByActivo('1');
		$this->Session->write('fase_actual', 1);
		$this->Session->write('etapas', 0);
		$this->Session->write('hay_empate', 0);
		$this->Session->write('hay_ganador', 0);
		$tiene_preguntas = 0;
		if ($torneo) {
			$tiene_preguntas = sizeof($torneo['Pregunta']);
		}
		$this->set(compact('torneo', 'tiene_preguntas'));
	}

/**
 * eliminatorias method
 *
 * @return void
 */
	public function eliminatorias() {
		$this->Session->write('ronda_actual', 1);
		$this->Torneo->recursive = 0;
		$torneo = $this->Torneo->findByActivo('1');
		if (!$torneo) {
			$this->redirect(array('action' => 'activo'));
		}
		// $this->Session->write('etapas', 0);die;
		// $this->Session->write('preguntas_realizadas', 6);
		// $this->Session->write('total_preguntas', 3);
		// $this->Session->write('fase_actual', 1);
		// die;
		// Obteniendo los equipos que estan participando
		$equipos = $this->Torneo->Equipo->getEquiposWithCountByTorneoId($torneo['Torneo']['id'], $this->Session->read('fase_actual'));
		// print_r($equipos);die;
		// Si no hay ninguno participando, tomamos los 3 primeros, segun la ronda en la que estamos
		if (!$equipos) {
			$equipos = $this->Torneo->Equipo->getEquiposNoParticipandoWithCountByTorneoId($torneo['Torneo']['id'], $this->Session->read('fase_actual'));
		}
		$hay_turno = false;
		foreach ($equipos as $equipo) {
			if ($equipo['Equipo']['tiene_turno']) {
				$hay_turno = true;
				break;
			}
		}
		if (!$hay_turno) {
			$equipos[0]['Equipo']['tiene_turno'] = 1;
			$this->Torneo->Equipo->save($equipos[0]);
		}

		// print_r($equipos);die;
		// echo sizeof($equipos) . " " ; print_r($equipos);
		if (!$this->Session->read('etapas')) {
			$this->Session->write('contador_aux', 0);
			$this->Session->write('fase_actual', 1);
			$equipos_torneo = $this->Torneo->Equipo->getAllEquiposWithCountByTorneoId($torneo['Torneo']['id']);
			$this->Session->write('etapas', ceil(sizeof($equipos_torneo) / Configure::read('minimo_equipos')));
			$this->Session->write('preguntas_realizadas', 0);
			$this->Session->write('total_preguntas', Configure::read('preguntas_eliminatoria') * sizeof($equipos));
			// $this->Session->write('hay_empate', 0);
			$this->redirect(array('action' => 'eliminatorias'));
		}
		$this->Session->write('preguntas_realizadas', $this->Torneo->Pregunta->Respuesta->getCountByTorneoId($torneo['Torneo']['id'], $this->Session->read('fase_actual')));

		// echo sprintf("equipos_fase: %s, etapas: %s, fase_actual: %s, preguntas_realizadas: %s, total_preguntas: %s<br/>", sizeof($equipos), $this->Session->read('etapas'), $this->Session->read('fase_actual'), $this->Session->read('preguntas_realizadas'), $this->Session->read('total_preguntas'));
		error_log(sprintf("equipos_fase: %s, etapas: %s, fase_actual: %s, preguntas_realizadas: %s, total_preguntas: %s<br/>", sizeof($equipos), $this->Session->read('etapas'), $this->Session->read('fase_actual'), $this->Session->read('preguntas_realizadas'), $this->Session->read('total_preguntas')));
		// die;
		if ($this->Session->read('total_preguntas') == 0) {
			$this->Session->write('total_preguntas', Configure::read('preguntas_eliminatoria') * sizeof($equipos));
			$this->redirect(array('action' => 'eliminatorias'));
		}
		if ($this->Session->read('preguntas_realizadas') >= $this->Session->read('total_preguntas')) {
			// echo $this->Session->read('contador_aux');die;
			$ganadores = $this->Torneo->Equipo->getGanadores($torneo['Torneo']['id'], $this->Session->read('fase_actual'));
			// die;
			// print_r($ganadores);die;
			if (sizeof($ganadores) == 1) {
				$this->Session->write('contador_aux', 0);
				$this->Session->setFlash(sprintf("Excelente el equipo %s ha sido el ganador de la ronda %s <div class='ganador-notify'></div>", 'Equipo ' . $ganadores[0]['Participante']['nombre_color'], $this->Session->read('fase_actual')), 'flash_success');

				$equipos = $this->Torneo->Equipo->getAllEquiposWithCountByTorneoId($torneo['Torneo']['id'], $this->Session->read('fase_actual'));
				// echo "<br />" . Configure::read('preguntas_eliminatoria') . ' ' .  sizeof($equipos) . ' ' .  Configure::read('minimo_equipos') . ' <br />' ;
				// print_r(Configure::read('preguntas_eliminatoria') * (sizeof($equipos) / Configure::read('minimo_equipos')));
				$this->Session->write('total_preguntas', Configure::read('preguntas_eliminatoria') * (sizeof($equipos) / Configure::read('minimo_equipos')));
				$this->Session->write('preguntas_realizadas', 0);
				$ganadores[0]['Equipo']['participando'] = 0;
				$ganadores[0]['Equipo']['clasificado'] = 1;
				$this->Torneo->Equipo->save($ganadores[0]);
				$this->Session->write('hay_ganador', 1);
				$this->Session->write('ganador', $ganadores[0]);

				if ( $this->Session->read('etapas') == $this->Session->read('fase_actual')) {
					$this->Session->write('fase_actual', 1);
					$this->Torneo->Equipo->query("UPDATE equipos SET participando = 0, tiene_turno = 0 WHERE torneo_id = {$torneo['Torneo']['id']}");
					if ($this->Session->read('etapas') > 1) {
						$this->Session->write('etapas', 0);
						$this->redirect(array('action' => 'finales'));
					} else {
						$ganadores[0]['Equipo']['campeon'] = 1;
						$this->Torneo->Equipo->save($ganadores[0]);
						$this->redirect(array('action' => 'fin'));
					}
				}

				$this->Session->write('fase_actual', $this->Session->read('fase_actual') + 1);
			} else {
				$this->Session->write('hay_empate', 1);
				// echo $this->Session->read('total_preguntas') . ' ' . sizeof($ganadores);
				$this->Session->write('total_preguntas', $this->Session->read('total_preguntas') + sizeof($ganadores));
				// print_r(Hash::extract($ganadores, '{n}.Participante.denominacion'));die;
				$this->Torneo->Equipo->query("UPDATE equipos SET tiene_turno = 0 WHERE torneo_id = {$torneo['Torneo']['id']}");
				$this->Session->setFlash(__('Ha ocurrido un empate entre ' . String::toList(Hash::extract($ganadores, '{n}.Participante.denominacion'), 'y'). ' debe realizar una pregunta adicional a cada uno para determinar el ganador <div class="empate-notify"></div>'), 'flash_warning');
			}
			$this->redirect(array('action' => 'eliminatorias'));
		}
		$this->set(compact('torneo', 'equipos', 'rondas', 'ganadores'));
	}

/**
 * finales method
 *
 * @return void
 */
	public function finales() {
		$this->Session->write('ronda_actual', 2);
		$this->Torneo->recursive = 0;
		$torneo = $this->Torneo->findByActivo('1');
		if (!$torneo) {
			$this->redirect(array('action' => 'activo'));
		}
		// $this->Session->write('etapas', 0);die;
		// $this->Session->write('preguntas_realizadas', 6);die;
		 // $this->Session->write('total_preguntas', 3);die;
		// $this->Session->write('fase_actual', 1);die;
		$equipos = $this->Torneo->Equipo->getEquiposWithCountByTorneoId($torneo['Torneo']['id'], $this->Session->read('fase_actual'), 2);
		// print_r($equipos);die;
		if (!$this->Session->read('etapas')) {
			$this->Session->write('contador_aux', 0);
			// die;
			$this->Torneo->Equipo->query("UPDATE equipos SET ronda_id = 2 WHERE clasificado = 1");
			if (!$equipos) {
				$equipos = $this->Torneo->Equipo->getEquiposNoParticipandoWithCountByTorneoId($torneo['Torneo']['id'], $this->Session->read('fase_actual'), 2);
				// print_r($equipos);die;
			}
			$equipos_torneo = $this->Torneo->Equipo->getAllEquiposWithCountByTorneoId($torneo['Torneo']['id'], 2);
			$this->Session->write('etapas', ceil(sizeof($equipos_torneo) / Configure::read('minimo_equipos')));
			$this->Session->write('preguntas_realizadas', 0);
			$this->Session->write('total_preguntas', Configure::read('preguntas_final') * sizeof($equipos));
		}
		$hay_turno = false;
		foreach ($equipos as $equipo) {
			if ($equipo['Equipo']['tiene_turno']) {
				$hay_turno = true;
				break;
			}
		}
		if (!$hay_turno) {
			$equipos[0]['Equipo']['tiene_turno'] = 1;
			$this->Torneo->Equipo->save($equipos[0]);
		}
		// die;
		$this->Session->write('preguntas_realizadas', $this->Torneo->Pregunta->Respuesta->getCountByTorneoId($torneo['Torneo']['id'], $this->Session->read('fase_actual'), 2));

		// echo sprintf("ronda_actual: %s, equipos_fase: %s, etapas: %s, fase_actual: %s, preguntas_realizadas: %s, total_preguntas: %s<br/>", $this->Session->read('ronda_actual'), sizeof($equipos), $this->Session->read('etapas'), $this->Session->read('fase_actual'), $this->Session->read('preguntas_realizadas'), $this->Session->read('total_preguntas'));
		// die;
		error_log(sprintf("FINALES ronda_actual: %s, equipos_fase: %s, etapas: %s, fase_actual: %s, preguntas_realizadas: %s, total_preguntas: %s<br/>", $this->Session->read('ronda_actual'), sizeof($equipos), $this->Session->read('etapas'), $this->Session->read('fase_actual'), $this->Session->read('preguntas_realizadas'), $this->Session->read('total_preguntas')));
		if ($this->Session->read('total_preguntas') == 0) {
			$this->Session->write('total_preguntas', Configure::read('preguntas_eliminatoria') * sizeof($equipos));
			$this->redirect(array('action' => 'eliminatorias'));
		}
		if ($this->Session->read('preguntas_realizadas') >= $this->Session->read('total_preguntas')) {
			$ganadores = $this->Torneo->Equipo->getGanadores($torneo['Torneo']['id'], $this->Session->read('fase_actual'), 2);
			// print_r($ganadores);die;
			if (sizeof($ganadores) == 1) {
				$this->Session->write('hay_ganador', 1);
				$this->Session->write('ganador', $ganadores[0]);

				$this->Session->write('contador_aux', 0);
				$this->Session->setFlash(sprintf("Excelente el equipo %s ha sido el Campeón", 'Equipo ' . $ganadores[0]['Participante']['nombre_color']), 'flash_success');
				// if ( $this->Session->read('etapas') == $this->Session->read('fase_actual')) {
					// $this->Session->write('etapas', 0);
					$this->Torneo->Equipo->query("UPDATE equipos SET participando = 0, tiene_turno = 0 WHERE torneo_id = {$torneo['Torneo']['id']}");
				// }
				// $this->Session->write('fase_actual', $this->Session->read('fase_actual') + 1);
				// $this->Session->write('preguntas_realizadas', 0);
				$ganadores[0]['Equipo']['participando'] = 0;
				$ganadores[0]['Equipo']['campeon'] = 1;
				$this->Torneo->Equipo->save($ganadores[0]);
				$this->redirect(array('action' => 'fin'));

				$this->Session->write('fase_actual', $this->Session->read('fase_actual') + 1);
				// $equipos = $this->Torneo->Equipo->getAllEquiposWithCountByTorneoId($torneo['Torneo']['id'], $this->Session->read('fase_actual'));
				// $this->Session->write('total_preguntas', Configure::read('preguntas_final') * (sizeof($equipos) / Configure::read('minimo_equipos')));
				// print_r($equipos);die;
				// $this->Session->write('hay_empate', 0);
			} else {
				$this->Session->write('hay_empate', 1);
				// echo $this->Session->read('total_preguntas') . ' ' . sizeof($ganadores);
				$this->Session->write('total_preguntas', $this->Session->read('total_preguntas') + sizeof($ganadores));
				$this->Torneo->Equipo->query("UPDATE equipos SET tiene_turno = 0 WHERE torneo_id = {$torneo['Torneo']['id']}");
				$this->Session->setFlash(__('Ha ocurrido un empate entre ' . String::toList(Hash::extract($ganadores, '{n}.Participante.denominacion'), 'y'). ' debe realizar una pregunta adicional a cada uno para determinar el ganador <div class="empate-notify"></div>'), 'flash_warning');
			}
			$this->redirect(array('action' => 'finales'));
		}

		$this->set(compact('torneo', 'equipos', 'rondas', 'ganadores'));
	}

/**
 * fin method
 *
 * @return void
 */
	public function fin() {
		$this->Torneo->recursive = 0;
		$torneo = $this->Torneo->findByActivo('1');
		if (!$torneo) {
			$this->redirect(array('action' => 'activo'));
		}
		$torneo['Torneo']['activo'] = 0;
		$this->Torneo->save($torneo);
		$this->Torneo->Equipo->recursive = -1;
		$ganadores = $this->Torneo->Equipo->getAllEquiposWithCountByTorneoId($torneo['Torneo']['id'], $this->Session->read('ronda_actual'), " ORDER BY Equipo__count DESC");
		$this->set(compact('ganadores'));
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Torneo->recursive = 0;
		$this->set('torneos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Torneo->exists($id)) {
			throw new NotFoundException(__('Invalid torneo'));
		}
		$options = array('conditions' => array('Torneo.' . $this->Torneo->primaryKey => $id));
		$this->set('torneo', $this->Torneo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Torneo->create();
			if ($this->Torneo->save($this->request->data)) {
				$torneo_id = $this->Torneo->getLastInsertID();
				$this->Torneo->query("UPDATE torneos SET activo = 0, iniciado = 0 WHERE NOT id = $torneo_id");
				$this->Session->setFlash(__('El torneo a sido creado'), 'flash_success');
				return $this->redirect(array('action' => 'activo'));
			} else {
				$this->Session->setFlash(__('El torneo no pudo ser creado, intente de nuevo'), 'flash_error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Torneo->exists($id)) {
			throw new NotFoundException(__('Invalid torneo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Torneo->save($this->request->data)) {
				$this->Session->setFlash(__('El torneo ha sido actualizado'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El torneo no se pudo actualizar, intente de nuevo'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Torneo.' . $this->Torneo->primaryKey => $id));
			$this->request->data = $this->Torneo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Torneo->id = $id;
		if (!$this->Torneo->exists()) {
			throw new NotFoundException(__('Invalid torneo'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Torneo->delete()) {
			$this->Session->setFlash(__('Torneo eliminado'), 'flash_success');
		} else {
			$this->Session->setFlash(__('No se pudo eliminar, intente de nuevo'), 'flash_error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
