<?php
App::uses('AppController', 'Controller');
/**
 * Respuestas Controller
 *
 * @property Respuesta $Respuesta
 * @property PaginatorComponent $Paginator
 */
class RespuestasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * mensaje method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function mensaje($es_correcta, $pregunta_id, $equipo_id) {
		$this->Respuesta->Equipo->recursive = 0;
		$equipo = $this->Respuesta->Equipo->findById($equipo_id);
		$pregunta = $this->Respuesta->Pregunta->findById($pregunta_id);
		$alternativa = $this->Respuesta->Pregunta->Alternativa->findByPreguntaIdAndOpcion($pregunta_id, $pregunta['Pregunta']['respuesta']);
		$this->set(compact('es_correcta', 'pregunta', 'alternativa', 'equipo'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($pregunta_id, $equipo_id, $nro_pregunta) {
		$pregunta = $this->Respuesta->Pregunta->findById($pregunta_id);
		if ($this->request->is('post')) {
			$this->request->data['Respuesta']['es_correcta'] = $pregunta['Pregunta']['respuesta'] == $this->request->data['Respuesta']['respuesta'];
			$this->Respuesta->create();
			if ($this->Respuesta->save($this->request->data)) {
				$this->Respuesta->Equipo->doNextTurno($equipo_id);
				$this->Session->write('contador_aux', ($this->Session->read('contador_aux') + 1));
				if ($this->request->data['Respuesta']['es_correcta']) {
					return $this->redirect(array('action' => 'mensaje', 1, $pregunta_id, $equipo_id));
				} else {
					return $this->redirect(array('action' => 'mensaje', 0, $pregunta_id, $equipo_id));
				}
			} else {
				$this->Session->setFlash(__('La respuesta no se pudo registrar, intente de nuevo'), 'flash_error');
			}
		}
		$this->Respuesta->Pregunta->doBloquear($pregunta_id);
		$equipo = $this->Respuesta->Equipo->findById($equipo_id);
		$respuestas = array('A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D');
		$this->set(compact('equipo', 'pregunta', 'respuestas', 'nro_pregunta'));
	}
}
