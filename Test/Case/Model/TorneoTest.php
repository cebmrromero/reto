<?php
App::uses('Torneo', 'Model');

/**
 * Torneo Test Case
 *
 */
class TorneoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.torneo',
		'app.equipo',
		'app.ronda',
		'app.pregunta',
		'app.respuesta'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Torneo = ClassRegistry::init('Torneo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Torneo);

		parent::tearDown();
	}

}
