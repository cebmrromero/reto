<?php
App::uses('Ronda', 'Model');

/**
 * Ronda Test Case
 *
 */
class RondaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ronda',
		'app.equipo',
		'app.torneo',
		'app.respuesta',
		'app.pregunta'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ronda = ClassRegistry::init('Ronda');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ronda);

		parent::tearDown();
	}

}
