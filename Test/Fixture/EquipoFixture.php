<?php
/**
 * EquipoFixture
 *
 */
class EquipoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 400, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'participante_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'tiene_turno' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'torneo_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'ronda_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'key' => 'index'),
		'participando' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'fase' => array('type' => 'string', 'null' => false, 'default' => '1', 'length' => 1, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'clasificado' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'campeon' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_equipos_torneos_idx' => array('column' => 'torneo_id', 'unique' => 0),
			'fk_equipos_rondas1_idx' => array('column' => 'ronda_id', 'unique' => 0),
			'fk_equipos_participantes1_idx' => array('column' => 'participante_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'nombre' => 'Lorem ipsum dolor sit amet',
			'participante_id' => 1,
			'tiene_turno' => 1,
			'torneo_id' => 1,
			'ronda_id' => 1,
			'participando' => 1,
			'fase' => 'Lorem ipsum dolor sit ame',
			'clasificado' => 1,
			'campeon' => 1
		),
	);

}
