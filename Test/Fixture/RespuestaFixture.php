<?php
/**
 * RespuestaFixture
 *
 */
class RespuestaFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'equipo_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'pregunta_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'es_correcta' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_respuestas_equipos1_idx' => array('column' => 'equipo_id', 'unique' => 0),
			'fk_respuestas_preguntas1_idx' => array('column' => 'pregunta_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'equipo_id' => 1,
			'pregunta_id' => 1,
			'es_correcta' => 1
		),
	);

}
