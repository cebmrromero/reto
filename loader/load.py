import csv
import MySQLdb
 
DB_HOST = 'localhost'
DB_USER = 'root'
DB_PASS = 'root'
DB_NAME = 'retodb'
DB_TABLE = 'preguntas'
TORNEO_ID = 60

datos = [DB_HOST, DB_USER, DB_PASS, DB_NAME]
conn = MySQLdb.connect(*datos)
cursor = conn.cursor()
with open("preguntas2.csv", 'rb') as csvfile:
    lines = csv.reader(csvfile, delimiter=';', quotechar='"')
    for row in lines:
        pregunta = row[0]
	ronda_id = 1 if row[1] == 'ELIMINATORIA' else 2
	respuesta = row[2]
	opcion_a = row[3]
	opcion_b = row[4]
	opcion_c = row[5]
	opcion_d = row[6]

	tmpl = 'INSERT INTO %s VALUES (null, "%s", "%s", "%s", "%s", "0");'
	query = tmpl % (DB_TABLE, TORNEO_ID, ronda_id, pregunta, respuesta)
	print query
	cursor.execute(query)
	pregunta_id = conn.insert_id()
#	conn.commit()
	tmpl = '  INSERT INTO alternativas VALUES (NULL, "%s", "%s", "%s");'
	query = tmpl % (pregunta_id, 'A', opcion_a)
	print query
        cursor.execute(query)
#        conn.commit()
        tmpl = '  INSERT INTO alternativas VALUES (NULL, "%s", "%s", "%s");'
        query = tmpl % (pregunta_id, 'B', opcion_b)
        print query
        cursor.execute(query)
#        conn.commit()
        tmpl = '  INSERT INTO alternativas VALUES (NULL, "%s", "%s", "%s");'
        query = tmpl % (pregunta_id, 'C', opcion_c)
        print query
        cursor.execute(query)
#        conn.commit()
        tmpl = '  INSERT INTO alternativas VALUES (NULL, "%s", "%s", "%s");'
        query = tmpl % (pregunta_id, 'D', opcion_d)
        print query
        cursor.execute(query)
	conn.commit()

conn.close()

