var clock = null;
$(function () {
	// if ($(".finales-container").length || $(".eliminatorias-container").length || $(".seleccionar-pregunta").length || $(".agregar-respuesta").length) {
	// 	$(".logos").remove();
	// }

	if ($(".respuesta-countdown").length) {
		clock = $(".respuesta-countdown").FlipClock(tiempo, {
			countdown: true,
			clockFace: 'MinuteCounter',
			defaultLanguage: 'spanish',
			autoStart: false,
			callbacks: {
				stop: function() {
					// setTimeout(
						// function () {
							$(".respuesta-countdown").parents("form").submit();
						// }, 1000
					// )
				},
				interval: function() {
					my_time = clock.getTime().time;
					if (my_time == 10) {
						$("#timer_a")[0].pause()
						$("#timer_b")[0].play()
					}
					if (my_time == 0) {
						$("#timer_c")[0].play()
					}
					console.log(getGreenToRed(my_time * 100 / tiempo), my_time * 100 / tiempo);
					$("#counter-container").css({'background-color': getGreenToRed(my_time * 100 / tiempo)});
				}
			}
		});
		$(".flip-clock-label").remove();


		if ($(".selector-respuestas").length) {
			$(".selector-respuestas button").addClass("bg-darkBlue");
			$(".selector-respuestas button img").addClass("bg-cobalt");

			$(".selector-respuestas button").click(function () {
				$(".selector-respuestas button").addClass("bg-darkBlue");
				$(".selector-respuestas button").removeClass("warning");
				$("#RespuestaRespuesta").val($(this).attr("alt"));
				$(this).removeClass("bg-darkBlue").addClass("warning");

				$(".selector-respuestas button img").addClass("bg-cobalt");
				$(".selector-respuestas button img").removeClass("bg-darkOrange");
				$(this).find("img").removeClass("bg-cobalt").addClass("bg-darkOrange");
				// $("#timer_a")[0].pause()
				// clock.stop();
			})
		}

		$(".iniciar-tiempo").click(function () {
			clock.start();
			$("#timer_a")[0].play()
			$(this).hide();
		})
	}
    
    if ($("#EquipoAddForm").length) {
    	restrict_multiple(".ocultables");
    	color_change(".ocultables");
    }

    if ($(".empate-notify").length) {
		$.Dialog({
			overlay: true,
			shadow: true,
			flat: true,
			title: '¡ATENCIÓN!',
			content: '',
			onShow: function(_dialog){
				var content = _dialog.children('.content');
				content.html($("div.empate-notice").html());
			}
		});
    }

    if ($(".ganador-notify").length) {
		$.Dialog({
			overlay: true,
			shadow: true,
			flat: true,
			title: '¡EXCELENTE!',
			content: '',
			onShow: function(_dialog){
				var content = _dialog.children('.content');
				content.html($("div.ganador-notice").html());
			}
		});
    }

    if ($(".campeon-notice").length) {
		$.Dialog({
			overlay: true,
			shadow: true,
			flat: true,
			title: '¡FELICIDADES!',
			content: '',
			onShow: function(_dialog){
				var content = _dialog.children('.content');
				content.html($("div.campeon-notice").html());
			}
		});
    }

    if ($("div.teams button.active").length) {
    	// Source: http://jsfiddle.net/7xvGp/
	    var glower = $('div.teams button.active');
		window.setInterval(function() {  
			glower.toggleClass('glowing');
		}, 200);
	}

    function color_change(selector) {
    	$(selector).change(function () {
    		var value = $(this).val()
    		var url = $(selector).data("url");
    		var current = $(this);
    		if (value) {
    			$.ajax({
    				url: url,
    				data: { id: value },
    				dataType: 'jsonp',
    				success: function(data) {
    					current.addClass(data.Participante.color + ' strong');
    				}
    			})
    		}
    	})
    }

    function restrict_multiple(selector) {
        // Aqui establece el valor actual en su alt
        $(selector).each(function () {
            $(this).attr("alt", $(this).val());
        })
        $(selector).change(function () {
            $(selector + " option").removeClass("hidden");
            $(this).attr("alt", $(this).val())
            var selected = new Array();
            $(selector + " option:selected").each(function () {
                selected.push(this.value);
            })
            
            for (k in selected) {
                $(selector + "[alt!=" + selected[k] + "] option[value=" + selected[k] + "]").addClass("hidden")
            }
        })
        $(selector).each(function () { $(this).trigger("change"); })
    }

	function getGreenToRed(percent){
		r = Math.floor(255-(100-percent)*255/100);
		return 'rgb(255,'+r+','+r+')';
	}
})
function changeBackground(image) {
	$(function () {
		$("body").css({"background": "#fff url('" + BASE_URL + "img/" + image + "') repeat-y 0% 100% fixed", "background-size": "100%", "overflow": "auto"});
	})
}