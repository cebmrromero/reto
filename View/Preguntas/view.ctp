<div class="preguntas view">
<h2><?php echo __('Pregunta'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($pregunta['Pregunta']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Torneo'); ?></dt>
		<dd>
			<?php echo $this->Html->link($pregunta['Torneo']['nombre'], array('controller' => 'torneos', 'action' => 'view', $pregunta['Torneo']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ronda'); ?></dt>
		<dd>
			<?php echo $this->Html->link($pregunta['Ronda']['nombre'], array('controller' => 'rondas', 'action' => 'view', $pregunta['Ronda']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pregunta'); ?></dt>
		<dd>
			<?php echo h($pregunta['Pregunta']['pregunta']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Respuesta'); ?></dt>
		<dd>
			<?php echo h($pregunta['Pregunta']['respuesta']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bloqueada'); ?></dt>
		<dd>
			<?php echo h($pregunta['Pregunta']['bloqueada']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Pregunta'), array('action' => 'edit', $pregunta['Pregunta']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Pregunta'), array('action' => 'delete', $pregunta['Pregunta']['id']), null, __('Are you sure you want to delete # %s?', $pregunta['Pregunta']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Preguntas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pregunta'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Torneos'), array('controller' => 'torneos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Torneo'), array('controller' => 'torneos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rondas'), array('controller' => 'rondas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ronda'), array('controller' => 'rondas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Respuestas'), array('controller' => 'respuestas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Respuesta'), array('controller' => 'respuestas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Alternativas'), array('controller' => 'alternativas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Alternativa'), array('controller' => 'alternativas', 'action' => 'add')); ?> </li>
	</ul>
</div>
	<div class="related">
		<h3><?php echo __('Related Respuestas'); ?></h3>
	<?php if (!empty($pregunta['Respuesta'])): ?>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
		<dd>
	<?php echo $pregunta['Respuesta']['id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Equipo Id'); ?></dt>
		<dd>
	<?php echo $pregunta['Respuesta']['equipo_id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Pregunta Id'); ?></dt>
		<dd>
	<?php echo $pregunta['Respuesta']['pregunta_id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Respuesta'); ?></dt>
		<dd>
	<?php echo $pregunta['Respuesta']['respuesta']; ?>
&nbsp;</dd>
		<dt><?php echo __('Es Correcta'); ?></dt>
		<dd>
	<?php echo $pregunta['Respuesta']['es_correcta']; ?>
&nbsp;</dd>
		</dl>
	<?php endif; ?>
		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('Edit Respuesta'), array('controller' => 'respuestas', 'action' => 'edit', $pregunta['Respuesta']['id'])); ?></li>
			</ul>
		</div>
	</div>
	<div class="related">
	<h3><?php echo __('Related Alternativas'); ?></h3>
	<?php if (!empty($pregunta['Alternativa'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Pregunta Id'); ?></th>
		<th><?php echo __('Opcion'); ?></th>
		<th><?php echo __('Texto'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($pregunta['Alternativa'] as $alternativa): ?>
		<tr>
			<td><?php echo $alternativa['id']; ?></td>
			<td><?php echo $alternativa['pregunta_id']; ?></td>
			<td><?php echo $alternativa['opcion']; ?></td>
			<td><?php echo $alternativa['texto']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'alternativas', 'action' => 'view', $alternativa['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'alternativas', 'action' => 'edit', $alternativa['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'alternativas', 'action' => 'delete', $alternativa['id']), null, __('Are you sure you want to delete # %s?', $alternativa['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Alternativa'), array('controller' => 'alternativas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
