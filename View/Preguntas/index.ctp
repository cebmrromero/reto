<div class="container">
	<div class="grid fluid">
		<div class="row">
			<div class="offset3 span6 bg-white">
				<div class="panel">
				    <div class="panel-header">
				        PREGUNTAS :: LISTADO
				    </div>
				    <div class="panel-content">
						<table cellpadding="0" cellspacing="0" class="table hovered">
							<thead>
								<tr>
									<?php if (!$torneo): ?>
										<th><?php echo $this->Paginator->sort('torneo_id'); ?></th>
									<?php endif; ?>
									<th><?php echo $this->Paginator->sort('pregunta'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($preguntas as $pregunta): ?>
								<tr>
									<?php if (!$torneo): ?>
										<td>
											<?php echo $this->Html->link($pregunta['Torneo']['nombre'], array('controller' => 'torneos', 'action' => 'view', $pregunta['Torneo']['id'])); ?>
										</td>
									<?php endif; ?>
									<td>
										<?php echo $this->Html->link(h($pregunta['Pregunta']['pregunta']), array('action' => 'edit', $pregunta['Pregunta']['id'])); ?>&nbsp;</td>
									<td class="actions">
										<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $pregunta['Pregunta']['id']), null, __('Are you sure you want to delete # %s?', $pregunta['Pregunta']['id'])); ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<small class="text-center">
							<?php
							echo $this->Paginator->counter(array(
							'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
							));
							?>
						</small>
						<div class="pagination text-center">
							<ul>
								<?php
								echo $this->Paginator->prev('<i class="icon-previous"></i>', array('escape' => false, 'tag' => 'li'), null, array('escape' => false, 'class' => 'prev disabled', 'disabledTag' => 'a', 'tag' => 'li'));
								echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
								echo $this->Paginator->next('<i class="icon-next"></i>', array('escape' => false, 'tag' => 'li'), null, array('escape' => false, 'class' => 'next disabled', 'disabledTag' => 'a', 'tag' => 'li'));
								?>
							</ul>
						</div>
						<div class="submit place-left"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('controller' => 'torneos', 'action' => 'activo')); ?>'" class="btn bg-blue fg-white"  value="Inicio"></div>
						<div class="submit place-right"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('action' => 'add')); ?>'" class="btn bg-green fg-white"  value="Agregar preguntas"></div>
						<br />
					</div>
			    </div>
			</div>
		</div>
	</div>
</div>