<div class="container seleccionar-pregunta">
	<div class="grid fluid">
		<div class="row">
			<div class="span7 alpha chalk-text pizarra">
				<div class="panel">
				    <div class="panel-content">
						<div class="torneos index">
							<div class="preguntas rows">
								<?php $j = 0; ?>
								<div class="row">
									<?php foreach ($preguntas as $pregunta): ?>
										<span>
											<?php if ($pregunta['Pregunta']['bloqueada']): ?>
												<div class="badge-pregunta"><?php echo ++$j; ?></div>
											<?php else: ?>
												<?php echo $this->Html->link('<div class="badge-pregunta fg-white strong">' . ++$j . '</div>', array('controller' => 'respuestas', 'action' => 'add', $pregunta['Pregunta']['id'], $equipo_id, $j), array('escape' => false)); ?>
											<?php endif; ?>
										</span>
									<?php endforeach; ?>
									<br />
									<br />
									<br />
								</div>
							</div>
						</div>
				    </div>
			    </div>
			</div>
			<div class="span5 omega">
				<h1>Ahora selecciona una pregunta</h1>
			</div>
		</div>
	</div>
</div>


<?php echo $this->Html->scriptBlock('
	$(function () {
		changeBackground("bg3.png")
	})
');
?>