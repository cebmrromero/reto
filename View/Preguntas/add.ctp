<?php $nro_pregunta = rand(1, 999); $aux = array_keys($respuestas); $respuesta_correcta = $aux[rand(0, 3)] ?>
<div class="container">
	<div class="grid fluid">
		<div class="row">
			<div class="offset2 span8 bg-white">
				<div class="panel">
				    <div class="panel-header">
				        PREGUNTAS :: AGREGAR
				    </div>
				    <div class="panel-content">
						<?php echo $this->Form->create('Pregunta', array('inputDefaults' => array('class' => 'span12', 'div' => false, 'data-transform' => 'input-control'))); ?>
							<fieldset>
								<?php if ($ultima_pregunta): ?>
									<div class="balloon top padding10">
										<p><strong>Ultima pregunta:</strong> <?php echo $ultima_pregunta['Pregunta']['pregunta']; ?></p>
									</div>
								<?php endif; ?>

								<div class="row">
									<div class="alpha span6">
										<span class="span12 button bg-darkRed fg-white">Eliminatoria: <strong><?php echo $cantidad_eliminatoria; ?></strong> preguntas</span>
									</div>
									<div class="omega span6">
										<span class="span12 button bg-darkGreen fg-white">Final: <strong><?php echo $cantidad_final; ?></strong> preguntas</span>
									</div>
								</div>

								<div class="hidden">
									<?php echo $this->Form->input('torneo_id', array('type' => 'hidden', 'value' => $torneo['Torneo']['id'])); ?>
								</div>
								<br /><br />
								<legend><?php echo __('Indicar los datos para crear una pregunta'); ?></legend>
								<?php
									echo $this->Form->input('ronda_id', array('value' => 1, 'label' => 'Ronda en la cual sera posible tomar la pregunta'));
									echo $this->Form->input('pregunta', array('rows' => '3', 'label' => 'Texto de la pregunta', 'value' => '¿Esto es una Pregunta de pruebas nro ' . $nro_pregunta . ' para los alumnos y que la respondan de forma correcta es necesario? respuesta correcta es ' . $respuesta_correcta));
								?>
								<legend><?php echo __('Alternativas de respuesta a la pregunta'); ?></legend>
								<div class="hidden">
									<?php
										echo $this->Form->input('Alternativa.0.opcion', array('type' => 'hidden', 'value' => 'A'));
										echo $this->Form->input('Alternativa.1.opcion', array('type' => 'hidden', 'value' => 'B'));
										echo $this->Form->input('Alternativa.2.opcion', array('type' => 'hidden', 'value' => 'C'));
										echo $this->Form->input('Alternativa.3.opcion', array('type' => 'hidden', 'value' => 'D'));
									?>
								</div>
								<?php 
									echo $this->Form->input('Alternativa.0.texto', array('rows' => '2', 'value' => "Respuesta de pruebas " . rand(1, 999) . " a la pregunta $nro_pregunta", 'label' => 'Opcion A' ));
									echo $this->Form->input('Alternativa.1.texto', array('rows' => '2', 'value' => "Esto es otra respuesta " . rand(1, 999) . "  a la pregunta $nro_pregunta", 'label' => 'Opcion B' ));
									echo $this->Form->input('Alternativa.2.texto', array('rows' => '2', 'value' => "Esta " . rand(1, 999) . " tal vez sea la opcion coorrecta de la pregunta $nro_pregunta", 'label' => 'Opcion C' ));
									echo $this->Form->input('Alternativa.3.texto', array('rows' => '2', 'value' => "Nuevamente " . rand(1, 999) . " puede ser una opcion valida a la respuesta a la pregunta $nro_pregunta", 'label' => 'Opcion D' ));

									echo $this->Form->input('respuesta', array('value' => $respuesta_correcta, 'label' => 'Indicar la opcion correcta'));
								?>
							</fieldset>
							<div class="submit place-left"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('controller' => 'torneos', 'action' => 'activo')); ?>'" class="btn bg-blue fg-white"  value="Inicio"></div>
							<div class="submit place-right"><input type="submit" class="btn bg-green fg-white"  value="Guardar"></div>
							<div class="submit place-right"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('action' => 'index')); ?>'" class="btn bg-gray fg-white"  value="Listado"></div>
							</br />
						<?php echo $this->Form->end(); ?>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
