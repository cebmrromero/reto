<div class="container">
	<div class="grid fluid">
		<div class="row">
			<div class="offset3 span6 bg-white">
				<div class="panel">
				    <div class="panel-content">
						<?php echo $this->Form->create('Pregunta', array('inputDefaults' => array('class' => 'span12', 'div' => false, 'data-transform' => 'input-control'))); ?>
							<fieldset>
								<div class="hidden">
									<?php echo $this->Form->input('id'); ?>
									<?php echo $this->Form->input('torneo_id', array('type' => 'hidden')); ?>
								</div>
								<legend><?php echo __('Editar una pregunta'); ?></legend>
								<?php
									echo $this->Form->input('ronda_id', array('label' => 'Ronda en la cual sera posible tomar la pregunta'));
									echo $this->Form->input('pregunta', array('rows' => '2', 'label' => 'Texto de la pregunta'));
								?>
								<legend><?php echo __('Alternativas de respuesta a la pregunta'); ?></legend>
								<div class="hidden">
									<?php
										echo $this->Form->input('Alternativa.0.opcion', array('type' => 'hidden', 'value' => 'A'));
										echo $this->Form->input('Alternativa.1.opcion', array('type' => 'hidden', 'value' => 'B'));
										echo $this->Form->input('Alternativa.2.opcion', array('type' => 'hidden', 'value' => 'C'));
										echo $this->Form->input('Alternativa.3.opcion', array('type' => 'hidden', 'value' => 'D'));
									?>
								</div>
								<?php 
									echo $this->Form->input('Alternativa.0.texto', array('rows' => '1', 'label' => 'Opcion A' ));
									echo $this->Form->input('Alternativa.1.texto', array('rows' => '1', 'label' => 'Opcion B' ));
									echo $this->Form->input('Alternativa.2.texto', array('rows' => '1', 'label' => 'Opcion C' ));
									echo $this->Form->input('Alternativa.3.texto', array('rows' => '1', 'label' => 'Opcion D' ));

									echo $this->Form->input('respuesta', array('label' => 'Indicar la opcion correcta'));
								?>
							</fieldset>
							<div class="submit place-left"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('controller' => 'torneos', 'action' => 'activo')); ?>'" class="btn bg-blue fg-white"  value="Inicio"></div>
							<div class="submit place-right"><input type="submit" class="btn bg-green fg-white"  value="Guardar"></div>
							<div class="submit place-right"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('action' => 'index')); ?>'" class="btn bg-gray fg-white"  value="Listado"></div>
							</br />
						<?php echo $this->Form->end(); ?>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
