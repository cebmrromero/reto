<?php $finales = (isset($finales)) ? $finales : 0; ?>
<?php $equipos_torneo = $this->requestAction(array('controller' => 'equipos', 'action' => 'getEquiposForResumen', $finales)); ?>
<div class="panel">
    <div class="panel-content">
		<table cellpadding="0" cellspacing="0" class="pizarra fg-white table resumen">
			<thead>
				<tr>
					<th>Escuela</th>
					<!-- <th>Respuestas</th> -->
					<th>Correctas</th>
					<th>Incorrectas</th>
					<th>Puntos</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($equipos_torneo as $equipo): ?>
					<tr>
						<th><?php echo $equipo['Participante']['denominacion']; ?></th> 
						<!-- <th><?php echo sizeof($equipo['Respuesta']); ?></th> -->
						<th class="number-strong"><?php echo $equipo['Equipo']['count'] + $equipo['Equipo']['penalizacion']; ?></th>
						<th class="number-strong"><?php echo sizeof($equipo['Respuesta']) - $equipo['Equipo']['count']; ?></th>
						<th class="number-strong"><strong><?php echo $equipo['Equipo']['count']; ?></strong></th>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
