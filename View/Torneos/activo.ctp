<div class="container eliminatorias-container">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 alpha">
				<div class="panel">
				    <div class="panel-content">
						<div class="row">
							<div class="span2 alpha">
								<?php echo $this->Html->image('logo_cem.png', array('class' => 'logos-inner logo-cem')); ?>
							</div>
							<div class="span8">
								&nbsp;
							</div>
							<div class="span2 text-right omega">
								<?php echo $this->Html->image('logo_sncf.png', array('class' => 'logos-inner')); ?>
							</div>
						</div>
					</div>
			    </div>
		    </div>
	    </div>
    </div>
</div>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br />
<div class="container" style="padding: 10px 0;">
	<div class="grid no-margin fluid">
		<div class="row no-margin">
			<div class="offset8 span4 padding10 button bg-darkGreen fg-white strong" style="padding-bottom:14px; border:5px solid #fff !important;">
				<?php if ($torneo): ?>
					<?php if ($torneo['Torneo']['iniciado']): ?>
						<?php echo $this->Html->link('Jugar', array('action' => ($torneo['Torneo']['ronda_id'] == 2) ? 'finales' : 'eliminatorias'), array('style' => 'width: 100%; font-size:3.5em; line-height: 1em')); ?>
					<?php else: ?>
						<?php if (!$tiene_preguntas): ?>
							<?php echo $this->Html->link('Definir Preguntas', array('controller' => 'preguntas', 'action' => 'add'), array('style' => 'width: 100%; font-size:3.5em; line-height: 1em')); ?>
						<?php else: ?>
							<?php echo $this->Html->link('Definir Equipos', array('controller' => 'equipos', 'action' => 'add'), array('style' => 'width: 100%; font-size:3.5em; line-height: 1em')); ?>
						<?php endif; ?>
					<?php endif; ?>
				<?php else: ?>
					<?php echo $this->Html->link('Crear Torneo', array('action' => 'add'), array('style' => 'width: 100%; font-size:3.5em')); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<?php echo $this->Html->scriptBlock('
	$(function () {
		changeBackground("bg.png")
	})
');
?>
