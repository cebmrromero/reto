<div class="container seleccionar-pregunta">
	<div class="grid fluid">
		<div class="row">
							<div class="offset2 span9 text-center">
				    			<div class="row">
									<?php $lugares = array('1' => '1er Lugar', '2' => '2do Lugar', '3' => '3er Lugar'); ?>
									<div class="lugares lugar_2 text-center span4 text-center">
										<button class="finales-button <?php echo $ganadores[1]['Participante']['color'];?> span12 image-button">
											<?php echo $ganadores[1]['Participante']['denominacion']; ?>
										</button>
									</div>
									<div class="lugares lugar_1 text-center span4 text-center">
										<button class="finales-button <?php echo $ganadores[0]['Participante']['color'];?> span12 image-button">
											<?php echo $ganadores[0]['Participante']['denominacion']; ?>
										</button>
									</div>
									<div class="lugares lugar_3 text-center span4 text-center">
										<button class="finales-button <?php echo $ganadores[2]['Participante']['color'];?> span12 image-button">
											<?php echo $ganadores[2]['Participante']['denominacion']; ?>
										</button>
									</div>
								</div>
				    			<div class="row">
				    				<div class="span12 podio">
				    					<?php echo $this->Html->image('ganadores.png'); ?>
			    					</div>
								</div><br />
								<div class="submit place-right"><input type="button" onclick="document.location.href='<?php echo $this->Html->url(array('controller' => 'torneos', 'action' => 'activo')); ?>'" class="btn large success fg-white" value="Finalizar"></div>
							</div>
		</div>
	</div>
</div>
<div class="grid fluid campeon-notice hidden">
	<div class="row">
		<div class="span9 bg-white alpha padding10">
			<h3 class="text-center">¡EQUIPO CAMPEÓN!</h3>
			<div class="grid fluid">
				<div class="row">
					<div class="span8 offset2">
						<button class="empate-button <?php echo $ganadores[0]['Participante']['color'];?> span12 image-button">
						<?php echo $ganadores[0]['Participante']['denominacion']; ?>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Html->media('aplausos_2.mp3', array('id' => 'timer_d', 'autoplay')); ?>

<?php echo $this->Html->scriptBlock('
	$(function () {
		changeBackground("bg4.png")
	})
');
?>
