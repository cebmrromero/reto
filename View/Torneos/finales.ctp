<div class="container eliminatorias-container">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 alpha">
				<div class="panel">
				    <div class="panel-content">
						<div class="row">
							<div class="span2 alpha">
								<?php echo $this->Html->image('logo_cem.png', array('class' => 'logos-inner logo-cem')); ?>
							</div>
							<div class="span8">
								<h1 class="text-center">Final<br /><?php echo floor($this->Session->read('contador_aux') / sizeof($equipos)) + 1; ?>º Ronda de Preguntas</h1>
							</div>
							<div class="span2 text-right omega">
								<?php echo $this->Html->image('logo_sncf.png', array('class' => 'logos-inner')); ?>
							</div>
						</div>
					</div>
			    </div>
		    </div>
	    </div>
		<div class="row">
			<div class="span6 alpha">
				<div class="panel">
				    <div class="panel-content">
						<div class="torneos index">
							<?php $colores = array('primary' => 'bg-cobalt', 'danger' => 'bg-red', 'bg-darkGreen' => 'bg-green', 'bg-amber' => 'bg-yellow'); ?>
							<div class="teams rows">
								<?php $i = 0; ?>
								<?php foreach ($equipos as $equipo): ?>
									<div class="row">
										<?php echo $this->Html->image($equipo['Participante']['icono'], array('class' => 'icono_equipos')); ?>
										<button onclick="document.location.href = '<?php echo $this->Html->url(array('controller' => 'preguntas', 'action' => 'seleccionar', $equipo['Equipo']['id'])); ?>'" class="<?php echo ($equipo['Equipo']['tiene_turno']) ? 'active' : 'inactive'; ?> bg-white span12 image-button">
										<?php echo $equipo['Participante']['nombre_color']; ?>
										<?php echo $this->Html->image('right.png', array('class' => ($equipo['Participante']['color'] == 'bg-white fg-black') ? 'bg-gray' : $equipo['Participante']['color'] )); ?>
										</button>
									</div>
									<?php $i++; ?>
								<?php endforeach; ?>
							</div>
						</div>
				    </div>
			    </div>
			</div>
			<div class="span6 omega pizarra">
				<?php echo $this->element('torneos/resumen', array('finales' => $this->Session->read("ronda_actual") - 1)); ?>
			</div>
		</div>
	</div>
</div>
<?php if ($this->Session->read('hay_empate')): ?>
	<div class="grid fluid empate-notice hidden">
		<div class="row">
			<div class="span9 bg-white alpha padding10">
				<?php $i = 0; ?>
				<h3 class="text-center">Ha ocurrido un empate entre los siguientes equipos:</h3>
				<div class="grid fluid">
					<?php foreach ($equipos as $equipo): ?>
						<div class="row">
							<div class="span8 offset2">
								<button class="empate-button <?php echo $equipo['Participante']['color']; ?> span12 image-button">
								<?php echo $equipo['Participante']['nombre_color']; ?>
								</button>
							</div>
						</div>
						<?php $i++; ?>
					<?php endforeach; ?>
				</div>
				<br />
				<h4 class="text-center">Se procederá a realizar una pregunta adicional para determinar el equipo que clasificará a la siguiente fase</h4>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if ($this->Session->read('hay_ganador')): ?>
	<?php $ganador = $this->Session->read('ganador'); ?>
	<div class="grid fluid ganador-notice hidden">
		<div class="row">
			<div class="span9 bg-white alpha padding10">
				<h3 class="text-center">El equipo que clasifica a la fase final es:</h3>
				<div class="grid fluid">
					<div class="row">
						<div class="span8 offset2">
							<button class="empate-button <?php echo $ganador['Participante']['color'];?> span12 image-button">
							<?php echo $ganador['Participante']['nombre_color']; ?>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php unset($_SESSION['hay_ganador']); ?>
	<?php unset($_SESSION['ganador']); ?>
<?php endif; ?>
