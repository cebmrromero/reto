<div class="torneos view">
<h2><?php echo __('Torneo'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($torneo['Torneo']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($torneo['Torneo']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($torneo['Torneo']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Torneo'), array('action' => 'edit', $torneo['Torneo']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Torneo'), array('action' => 'delete', $torneo['Torneo']['id']), null, __('Are you sure you want to delete # %s?', $torneo['Torneo']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Torneos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Torneo'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Equipos'), array('controller' => 'equipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Equipo'), array('controller' => 'equipos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Preguntas'), array('controller' => 'preguntas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pregunta'), array('controller' => 'preguntas', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Equipos'); ?></h3>
	<?php if (!empty($torneo['Equipo'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('Torneo Id'); ?></th>
		<th><?php echo __('Ronda Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($torneo['Equipo'] as $equipo): ?>
		<tr>
			<td><?php echo $equipo['id']; ?></td>
			<td><?php echo $equipo['nombre']; ?></td>
			<td><?php echo $equipo['torneo_id']; ?></td>
			<td><?php echo $equipo['ronda_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'equipos', 'action' => 'view', $equipo['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'equipos', 'action' => 'edit', $equipo['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'equipos', 'action' => 'delete', $equipo['id']), null, __('Are you sure you want to delete # %s?', $equipo['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Equipo'), array('controller' => 'equipos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Preguntas'); ?></h3>
	<?php if (!empty($torneo['Pregunta'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Torneo Id'); ?></th>
		<th><?php echo __('Ronda Id'); ?></th>
		<th><?php echo __('Pregunta'); ?></th>
		<th><?php echo __('Respuesta'); ?></th>
		<th><?php echo __('Bloqueada'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($torneo['Pregunta'] as $pregunta): ?>
		<tr>
			<td><?php echo $pregunta['id']; ?></td>
			<td><?php echo $pregunta['torneo_id']; ?></td>
			<td><?php echo $pregunta['ronda_id']; ?></td>
			<td><?php echo $pregunta['pregunta']; ?></td>
			<td><?php echo $pregunta['respuesta']; ?></td>
			<td><?php echo $pregunta['bloqueada']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'preguntas', 'action' => 'view', $pregunta['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'preguntas', 'action' => 'edit', $pregunta['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'preguntas', 'action' => 'delete', $pregunta['id']), null, __('Are you sure you want to delete # %s?', $pregunta['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Pregunta'), array('controller' => 'preguntas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
