<div class="container">
	<div class="grid fluid">
		<div class="row">
			<div class="offset3 span6 bg-white">
				<div class="panel">
				    <div class="panel-header">
				        TORNEOS :: EDITAR
				    </div>
				    <div class="panel-content">
						<?php echo $this->Form->create('Torneo', array('inputDefaults' => array('class' => 'span12', 'div' => false))); ?>
							<?php echo $this->Form->input('id'); ?>
							<fieldset>
								<legend><?php echo __('Indicar los datos para crear el torneo'); ?></legend>
								<?php echo $this->Form->input('nombre', array('data-transform' => 'input-control')); ?>
								<?php echo $this->Form->input('ronda_id', array('type' => 'hidden', 'value' => 1));?>
							</fieldset>
							<div class="submit place-left"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('controller' => 'torneos', 'action' => 'activo')); ?>'" class="btn bg-blue fg-white"  value="Inicio"></div>
							<div class="submit place-right"><input type="submit" class="btn bg-green fg-white"  value="Guardar"></div>
							</br />
						<?php echo $this->Form->end(); ?>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>