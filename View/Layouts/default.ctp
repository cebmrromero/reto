<?php $cakeDescription = __d('cake_dev', 'Reto al Conocimiento'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="product" content="Metro UI CSS Framework">
		<meta name="description" content="Simple responsive css framework">
		<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">
		<meta name="keywords" content="js, css, metro, framework, windows 8, metro ui">
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->Html->css(array(
				'metro-bootstrap',
				'metro-bootstrap-responsive',
				'docs',
				'prettify',
				'flipclock',
				'main',
			));
		
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->Html->scriptBlock('var BASE_URL = "/reto/"');
		?>
		<title><?php echo $cakeDescription ?>: <?php echo $title_for_layout; ?></title>
	</head>
	<body class="metro" style="background-color: #efeae3">
		<?php $auth_user = $this->Session->read("Auth.User"); ?>
		<div class="">
			<?php //if ($this->Session->check('Message.flash')): ?>
				<div class="container hidden">
					<div class="grid fluid">
						<div class="row no-margin logos">
							<div class="span6">
								<?php //echo $this->Html->image('logo_cem.png', array('class' => 'logo_cem')); ?>
							</div>
							<div class="span6 text-right">
								<?php //echo $this->Html->image('logo_reto.png', array('class' => 'logo_reto')); ?>
							</div>
						</div>
						<!-- <div class="row logos">
							<div class="text-center">
							</div>
						</div> -->
 						<div class="row">
							<div class="span8 offset2 text-center">
								<?php echo $this->Session->flash(); ?>
								<?php echo $this->Session->flash('auth'); ?>
							</div>
						</div>
					</div>
				</div>
			<?php //endif; ?>
			
			<?php 
			echo $this->Html->script(array(
				'jquery.min',
				'jquery.widget.min',
				'jquery.mousewheel',
				'prettify',
				'metro.min',
				'docs',
				'base',
				'flipclock',
				'main',
			));
			echo $this->fetch('script');
			echo $this->fetch('content'); ?>
		</div>
	</body>
</html>
