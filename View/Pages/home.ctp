<div id="top-image">
	<div class="container" style="padding: 50px 20px">
		<h1 class="fg-black bigger">Reto al Conocimiento</h1>
		<h2 class="fg-black bigger">
			<?php echo __("Confias en tu conocimiento?") ?><br />
			<?php echo __("Te retamos.") ?>
		</h2>
	</div>
</div>
<br /><br /><br />
<div class="container" style="padding: 10px 0;">
	<div class="grid no-margin">
		<div class="row no-margin">
			<div class="span4 offset5 padding10 button success" style="padding-bottom:14px">
				<br />
				<!-- <a class="button success " style="width: 100%; margin-bottom: 5px"  href="http://192.168.0.134/trac/sac/">Trac</a>
				<a class="button info " style="width: 100%; margin-bottom: 5px"  href="http://192.168.0.134/svn/sac/">Subversion</a> -->
				<?php echo $this->Html->link('Comenzar', array('controller' => 'torneos', 'action' => 'add'), array('style' => 'width: 100%; font-size:3.5em')); ?>
			</div>
		</div>
	</div>
</div>
<br /><br /><br /><br /><br /><br /><br /><br />

<div class="span12 padding10">
	<?php echo $this->Html->link('Configurar Equipos', array('controller' => 'equipos', 'action' => 'add'), array('class' => 'padding10 button success')); ?>
	<?php echo $this->Html->link('Definir Preguntas', array('controller' => 'preguntas', 'action' => 'add'), array('class' => 'padding10 button warning')); ?>
</div>