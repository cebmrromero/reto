<div class="container">
	<div class="grid fluid">
		<div class="row">
			<div class="offset1 span10">
				<div class="panel">
				    <div class="panel-content">
						<?php if ($es_correcta): ?>
							<h2 class="text-center exito-h2 span9">¡Excelente!<br /> Equipo <?php echo $equipo['Participante']['nombre_color']; ?>, su respuesta es correcta</h2>
							<p class="text-center">
								<?php echo $this->Html->image('exito.png'); ?>
							</p>

							<?php echo $this->Html->media('aplausos_2.mp3', array('id' => 'timer_d', 'autoplay')); ?>
						<?php else: ?>
							<h2 class="text-center exito-h2 span9">¡Lo sentimos!<br />Respuesta incorrecta</h2>
							<h3 class="text-center perder-h2 span9">
								Equipo <?php echo $equipo['Participante']['nombre_color']; ?>, la respuesta correcta era la Opción <?php echo $alternativa['Alternativa']['opcion']; ?>:<br />
								<strong><?php echo $alternativa['Alternativa']['texto']; ?></strong>
							</h3>
							<p class="text-center">
								<?php echo $this->Html->image('perder.png'); ?>
							</p>

							<?php echo $this->Html->media('error.mp3', array('id' => 'timer_c', 'autoplay')); ?>
						<?php endif; ?>
						<br />
						<div class="submit place-right"><input type="button" onclick="document.location.href='<?php echo $this->Html->url(array('controller' => 'torneos', 'action' => ($this->Session->read('ronda_actual') == 2) ? 'finales' : 'eliminatorias')); ?>'" class="btn large success fg-white" value="Continuar"></div>
						<br />
						<br />
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $this->Html->scriptBlock('
	$(function () {
		changeBackground("bg2.png")
	})
');
?>
