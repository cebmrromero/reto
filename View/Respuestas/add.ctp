<div class="container agregar-respuesta">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 alpha">
				<div class="panel">
				    <div class="panel-content">
				    	<h1 class="text-center">Equipo <?php echo ucfirst($equipo['Participante']['nombre_color']); ?><br />Pregunta seleccionada Nro: <?php echo $nro_pregunta; ?></h1>
				    	<br />
						<?php echo $this->Form->create('Respuesta', array('inputDefaults' => array('class' => 'span12', 'div' => false, 'data-transform' => 'input-control'))); ?>
							<h2 class="pregunta"><?php echo $pregunta['Pregunta']['pregunta']; ?></h2>
							<div class="row">
								<div id="counter-container" class="span12 text-center padding10">
									<div class="respuesta-countdown"></div>
									<button type="button" class="btn bg-red fg-white large strong iniciar-tiempo">¡TIEMPO!</button>
								</div>
							</div>
							<?php $i = 0; ?>
							<?php foreach ($pregunta['Alternativa'] as $alternativa): $aux = array_keys($respuestas); ?>
								<div class="row selector-respuestas">
									<div class="span12">
										<button type="button" class="span12 image-button image-left fg-white" alt="<?php echo $aux[$i]; ?>">
											<?php echo $alternativa['texto']; ?>
											<?php echo $this->Html->image(strtolower($aux[$i]) .  '.png', array('class' => 'bg-cobalt')); ?>
										</button>
									</div>
								</div>
								<?php $i++; ?>
							<?php endforeach; ?>
							<div class="hidden">
								<?php
								echo $this->Form->input('equipo_id', array('type' => 'hidden', 'value' => $equipo['Equipo']['id']));
								echo $this->Form->input('ronda_id', array('type' => 'hidden', 'value' => $this->Session->read('ronda_actual')));
								echo $this->Form->input('pregunta_id', array('type' => 'hidden', 'value' => $pregunta['Pregunta']['id']));
								echo $this->Form->input('fase', array('type' => 'hidden', 'value' => $this->Session->read('fase_actual')));
								echo $this->Form->input('respuesta', array('type' => 'text'));
								?>
							</div>

							</br />
							</br />
							<div class="submit place-left"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('controller' => 'preguntas', 'action' => 'seleccionar', $equipo['Equipo']['id'])) ?>'" class="btn large bg-darkRed fg-white" value="¡ANULAR RESPUESTA!"></div>
							<div class="submit place-right"><input type="submit" class="btn large bg-orange fg-white" value="¡RESPONDER!"></div>
							</br />
						<?php echo $this->Form->end(); ?>
				    </div>
			    </div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Html->media('timer.mp3', array('id' => 'timer_a')); ?>
<?php echo $this->Html->media('ultimos.mp3', array('id' => 'timer_b')); ?>
<?php echo $this->Html->media('error.mp3', array('id' => 'timer_c')); ?>
<?php if ($this->Session->read('ronda_actual') == 2): ?>
	<?php echo $this->Html->scriptBlock(' var tiempo = 30'); ?>
<?php else: ?>
	<?php echo $this->Html->scriptBlock(' var tiempo = 30'); ?>
<?php endif; ?>

<?php echo $this->Html->scriptBlock('
	$(function () {
		changeBackground("bg2.png")
	})
');
?>
