<div class="respuestas view">
<h2><?php echo __('Respuesta'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($respuesta['Respuesta']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipo'); ?></dt>
		<dd>
			<?php echo $this->Html->link($respuesta['Equipo']['nombre'], array('controller' => 'equipos', 'action' => 'view', $respuesta['Equipo']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pregunta'); ?></dt>
		<dd>
			<?php echo $this->Html->link($respuesta['Pregunta']['pregunta'], array('controller' => 'preguntas', 'action' => 'view', $respuesta['Pregunta']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Respuesta'); ?></dt>
		<dd>
			<?php echo h($respuesta['Respuesta']['respuesta']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Es Correcta'); ?></dt>
		<dd>
			<?php echo h($respuesta['Respuesta']['es_correcta']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Respuesta'), array('action' => 'edit', $respuesta['Respuesta']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Respuesta'), array('action' => 'delete', $respuesta['Respuesta']['id']), null, __('Are you sure you want to delete # %s?', $respuesta['Respuesta']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Respuestas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Respuesta'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Equipos'), array('controller' => 'equipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Equipo'), array('controller' => 'equipos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Preguntas'), array('controller' => 'preguntas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pregunta'), array('controller' => 'preguntas', 'action' => 'add')); ?> </li>
	</ul>
</div>
