<div class="participantes view">
<h2><?php echo __('Participante'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($participante['Participante']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Denominacion'); ?></dt>
		<dd>
			<?php echo h($participante['Participante']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Color'); ?></dt>
		<dd>
			<?php echo h($participante['Participante']['color']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre Color'); ?></dt>
		<dd>
			<?php echo h($participante['Participante']['nombre_color']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Torneo'); ?></dt>
		<dd>
			<?php echo $this->Html->link($participante['Torneo']['nombre'], array('controller' => 'torneos', 'action' => 'view', $participante['Torneo']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Participante'), array('action' => 'edit', $participante['Participante']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Participante'), array('action' => 'delete', $participante['Participante']['id']), null, __('Are you sure you want to delete # %s?', $participante['Participante']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Participantes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Participante'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Torneos'), array('controller' => 'torneos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Torneo'), array('controller' => 'torneos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Equipos'), array('controller' => 'equipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Equipo'), array('controller' => 'equipos', 'action' => 'add')); ?> </li>
	</ul>
</div>
	<div class="related">
		<h3><?php echo __('Related Equipos'); ?></h3>
	<?php if (!empty($participante['Equipo'])): ?>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
		<dd>
	<?php echo $participante['Equipo']['id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
	<?php echo $participante['Equipo']['nombre']; ?>
&nbsp;</dd>
		<dt><?php echo __('Participante Id'); ?></dt>
		<dd>
	<?php echo $participante['Equipo']['participante_id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Tiene Turno'); ?></dt>
		<dd>
	<?php echo $participante['Equipo']['tiene_turno']; ?>
&nbsp;</dd>
		<dt><?php echo __('Torneo Id'); ?></dt>
		<dd>
	<?php echo $participante['Equipo']['torneo_id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Ronda Id'); ?></dt>
		<dd>
	<?php echo $participante['Equipo']['ronda_id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Participando'); ?></dt>
		<dd>
	<?php echo $participante['Equipo']['participando']; ?>
&nbsp;</dd>
		<dt><?php echo __('Fase'); ?></dt>
		<dd>
	<?php echo $participante['Equipo']['fase']; ?>
&nbsp;</dd>
		<dt><?php echo __('Clasificado'); ?></dt>
		<dd>
	<?php echo $participante['Equipo']['clasificado']; ?>
&nbsp;</dd>
		<dt><?php echo __('Campeon'); ?></dt>
		<dd>
	<?php echo $participante['Equipo']['campeon']; ?>
&nbsp;</dd>
		</dl>
	<?php endif; ?>
		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('Edit Equipo'), array('controller' => 'equipos', 'action' => 'edit', $participante['Equipo']['id'])); ?></li>
			</ul>
		</div>
	</div>
	