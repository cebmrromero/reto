<div class="container">
	<div class="grid fluid">
		<div class="row">
			<div class="offset3 span6 bg-white">
				<div class="panel">
				    <div class="panel-content">
						<?php echo $this->Form->create('Equipo', array('inputDefaults' => array('class' => 'span12', 'div' => false, 'data-transform' => 'input-control'))); ?>
							<fieldset>
								<legend><?php echo __('Editar equipo'); ?></legend>
									<?php echo $this->Form->input('nombre', array('label' => false, 'placeholder' => 'Nombre del equipo')); ?>
									<?php echo $this->Form->input('penalizacion', array('label' => 'Puntos de penalización', 'placeholder' => 'Penalizacion', 'class' => 'fg-red')); ?>
									<div class="hidden">
										<?php
										echo $this->Form->input('id', array('type' => 'hidden'));
										echo $this->Form->input('torneo_id', array('type' => 'hidden'));
										echo $this->Form->input('ronda_id', array('type' => 'hidden'));
										echo $this->Form->input('tiene_turno', array('type' => 'hidden'));
										?>
									</div>
							</fieldset>
							<div class="submit place-left"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('controller' => 'torneos', 'action' => 'activo')); ?>'" class="btn bg-blue fg-white"  value="Inicio"></div>
							<div class="submit place-right"><input type="submit" class="btn bg-green fg-white"  value="Guardar"></div>
							<div class="submit place-right"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('action' => 'index')); ?>'" class="btn bg-gray fg-white"  value="Listado"></div>
							</br />
						<?php echo $this->Form->end(); ?>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
