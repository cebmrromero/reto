<div class="container">
	<div class="grid fluid">
		<div class="row">
			<div class="offset2 span8 bg-white">
				<div class="panel">
				    <div class="panel-content">
						<?php echo $this->Form->create('Equipo', array('inputDefaults' => array('class' => 'span12', 'div' => false, 'data-transform' => 'input-control'))); ?>
							<fieldset>
								<legend><?php echo __('Sorteo de equipos'); ?></legend>
								<?php for ($i = 0; $i < $cantidad; $i++): ?>
									<?php echo $this->Form->input($i . '.participante_id', array('label' => false, 'empty' => 'Posición ' . ($i + 1), 'type' => 'select', 'class' => 'ocultables', 'options' => $participantes_options, 'data-url' => $this->Html->url(array('controller' => 'participantes', 'action' => 'getParticipanteColorByIdJSONP', $participantes[$i]['Participante']['id'])))); ?>
									<div class="hidden">
										<?php
										echo $this->Form->input($i . '.nombre', array('type' => 'hidden', 'value' => 'test'));
										echo $this->Form->input($i . '.torneo_id', array('type' => 'hidden', 'value' => $torneo['Torneo']['id']));
										echo $this->Form->input($i . '.ronda_id', array('type' => 'hidden', 'value' => '1'));
										echo $this->Form->input($i . '.tiene_turno', array('type' => 'hidden', 'value' => ($i == 0) ? 1 : 0));
										?>
									</div>
									<?php endfor; ?>
							</fieldset>
							<div class="submit place-left"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('controller' => 'torneos', 'action' => 'activo')); ?>'" class="btn bg-blue fg-white"  value="Inicio"></div>
							<div class="submit place-right"><input type="submit" class="btn bg-green fg-white"  value="Guardar"></div>
							<div class="submit place-right"><input type="button" onclick="document.location.href = '<?php echo $this->Html->url(array('action' => 'index')); ?>'" class="btn bg-gray fg-white"  value="Listado"></div>
							</br />
						<?php echo $this->Form->end(); ?>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
