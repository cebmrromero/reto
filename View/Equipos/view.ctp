<div class="equipos view">
<h2><?php echo __('Equipo'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($equipo['Equipo']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($equipo['Equipo']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Torneo'); ?></dt>
		<dd>
			<?php echo $this->Html->link($equipo['Torneo']['nombre'], array('controller' => 'torneos', 'action' => 'view', $equipo['Torneo']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ronda'); ?></dt>
		<dd>
			<?php echo $this->Html->link($equipo['Ronda']['nombre'], array('controller' => 'rondas', 'action' => 'view', $equipo['Ronda']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Equipo'), array('action' => 'edit', $equipo['Equipo']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Equipo'), array('action' => 'delete', $equipo['Equipo']['id']), null, __('Are you sure you want to delete # %s?', $equipo['Equipo']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Equipos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Equipo'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Torneos'), array('controller' => 'torneos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Torneo'), array('controller' => 'torneos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rondas'), array('controller' => 'rondas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ronda'), array('controller' => 'rondas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Respuestas'), array('controller' => 'respuestas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Respuesta'), array('controller' => 'respuestas', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Respuestas'); ?></h3>
	<?php if (!empty($equipo['Respuesta'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Equipo Id'); ?></th>
		<th><?php echo __('Pregunta Id'); ?></th>
		<th><?php echo __('Respuesta'); ?></th>
		<th><?php echo __('Es Correcta'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($equipo['Respuesta'] as $respuesta): ?>
		<tr>
			<td><?php echo $respuesta['id']; ?></td>
			<td><?php echo $respuesta['equipo_id']; ?></td>
			<td><?php echo $respuesta['pregunta_id']; ?></td>
			<td><?php echo $respuesta['respuesta']; ?></td>
			<td><?php echo $respuesta['es_correcta']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'respuestas', 'action' => 'view', $respuesta['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'respuestas', 'action' => 'edit', $respuesta['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'respuestas', 'action' => 'delete', $respuesta['id']), null, __('Are you sure you want to delete # %s?', $respuesta['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Respuesta'), array('controller' => 'respuestas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
