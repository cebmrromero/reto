<?php
App::uses('AppModel', 'Model');
/**
 * Respuesta Model
 *
 * @property Equipo $Equipo
 * @property Pregunta $Pregunta
 */
class Respuesta extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'equipo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'pregunta_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'es_correcta' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Equipo' => array(
			'className' => 'Equipo',
			'foreignKey' => 'equipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Pregunta' => array(
			'className' => 'Pregunta',
			'foreignKey' => 'pregunta_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Ronda' => array(
			'className' => 'Ronda',
			'foreignKey' => 'ronda_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getCountByTorneoId($torneo_id, $fase, $ronda_id = 1) {
		$sql = "SELECT COUNT( r.id ) as count FROM respuestas as r
			LEFT JOIN preguntas as p on p.id = r.pregunta_id
			WHERE p.torneo_id = %s AND r.fase = %s AND r.ronda_id = %s";
		$sql = sprintf($sql, $torneo_id, $fase, $ronda_id);
		$count = $this->query($sql);
		return $count[0][0]['count'];
	}
}
