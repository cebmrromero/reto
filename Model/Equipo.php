<?php
App::uses('AppModel', 'Model');
/**
 * Equipo Model
 *
 * @property Torneo $Torneo
 * @property Ronda $Ronda
 * @property Respuesta $Respuesta
 */
class Equipo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nombre' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'participante_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tiene_turno' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'torneo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ronda_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'participando' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fase' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'clasificado' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'campeon' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Participante' => array(
			'className' => 'Participante',
			'foreignKey' => 'participante_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Torneo' => array(
			'className' => 'Torneo',
			'foreignKey' => 'torneo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Ronda' => array(
			'className' => 'Ronda',
			'foreignKey' => 'ronda_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Respuesta' => array(
			'className' => 'Respuesta',
			'foreignKey' => 'equipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function doNextTurno($id) {
		$torneo = $this->Torneo->findByActivo('1');
		$equipo = $this->findByIdAndTorneoIdAndParticipando($id, $torneo['Torneo']['id'], 1);
		$equipo['Equipo']['tiene_turno'] = 0;
		$this->save($equipo);
		$equipo = $this->Torneo->Equipo->find('neighbors', array('field' => 'id', 'value' => $id, 'conditions' => array('Equipo.participando' => 1, 'Equipo.torneo_id' => $torneo['Torneo']['id'])));
		// print_r($equipo);die;
		$equipo = $equipo['next'];
		if (!$equipo) {
			$equipo = $this->findByTorneoId($torneo['Torneo']['id']);
		}
		if ($equipo['Equipo']['id']) {
			$equipo['Equipo']['tiene_turno'] = 1;
			$this->save($equipo);
		}
	}

	public function getAllEquiposWithCountByTorneoId($torneo_id, $ronda_id = 1, $order = "") {
		$this->virtualFields['count'] = 0;
		$sql = "SELECT
				Equipo.*, Participante.*, COUNT(Respuesta.id) - Equipo.penalizacion as Equipo__count, Equipo.penalizacion
			FROM equipos as Equipo
			LEFT JOIN participantes as Participante ON Participante.id = Equipo.participante_id
			LEFT JOIN respuestas as Respuesta on Respuesta.equipo_id = Equipo.id AND Respuesta.es_correcta = 1 AND Respuesta.ronda_id = %s
			WHERE Equipo.torneo_id = %s AND Equipo.ronda_id = %s
			GROUP BY Equipo.id %s";
		$sql = sprintf($sql, $ronda_id, $torneo_id, $ronda_id, $order);
		// echo $sql; die;
		return $this->query($sql);
	}

	// public function getEquiposSiguienteFaseWithCountByTorneoId($torneo_id, $fase) {
		
	// }

	public function getEquiposWithCountByTorneoId($torneo_id, $fase = null, $ronda_id = 1) {
		// ESTA FUNCION TENGO QUE CAMBIARLA TODA!!!! TODA TODA TODA TODA TODA!!!!!
		$and = '';
		if ($fase) {
			$and = " AND Equipo.fase = $fase ";
		}
		$this->virtualFields['count'] = 0;
		$sql = "SELECT
				Equipo.*, Participante.*, COUNT(Respuesta.id) - Equipo.penalizacion as Equipo__count, Equipo.penalizacion
			FROM equipos as Equipo
			LEFT JOIN participantes as Participante ON Participante.id = Equipo.participante_id
			LEFT JOIN respuestas as Respuesta on Respuesta.equipo_id = Equipo.id AND Respuesta.es_correcta = 1 AND Respuesta.ronda_id = %s
			WHERE Equipo.torneo_id = %s AND Equipo.ronda_id = %s AND participando = 1 $and
			GROUP BY Equipo.id";
		$sql = sprintf($sql, $ronda_id, $torneo_id, $ronda_id);
		// echo $sql; die;
		return $this->query($sql);
	}

	public function getEquiposNoParticipandoWithCountByTorneoId($torneo_id, $fase = null, $ronda_id = 1) {
		$range = Configure::read('minimo_equipos');
		$limit = '';
		if ($fase) {
			$aux = ($fase - 1) * $range;
			$limit = " LIMIT $aux, $range";
		}
		$this->virtualFields['count'] = 0;
		$sql = "SELECT
				Equipo.*, Participante.*, COUNT(Respuesta.id) - Equipo.penalizacion as Equipo__count, Equipo.penalizacion
			FROM equipos as Equipo
			LEFT JOIN participantes as Participante ON Participante.id = Equipo.participante_id
			LEFT JOIN respuestas as Respuesta on Respuesta.equipo_id = Equipo.id AND Respuesta.es_correcta = 1 AND Respuesta.ronda_id = %s
			WHERE Equipo.torneo_id = %s AND Equipo.ronda_id = %s
			GROUP BY Equipo.id %s";
		$sql = sprintf($sql, $ronda_id, $torneo_id, $ronda_id, $limit);
		// echo $sql; die;
		$equipos = $this->query($sql);

		foreach ($equipos as &$equipo) {
			$equipo['Equipo']['participando'] = 1;
			$equipo['Equipo']['fase'] = $fase;
			$this->save($equipo);
		}
		// print_r($equipos);die;
		// TODO: Esto lo comente recientemete si da problemas descomentarlo, es un fix jalado de los pelos
		// if (!$equipos) {
		// 	$sql = "SELECT
		// 			Equipo.*, COUNT(Respuesta.id) as Equipo__count
		// 		FROM equipos as Equipo
		// 		LEFT JOIN respuestas as Respuesta on Respuesta.equipo_id = Equipo.id AND Respuesta.es_correcta = 1
		// 		WHERE Equipo.torneo_id = %s AND Equipo.ronda_id = %s AND participando = 1 $and
		// 		GROUP BY Equipo.id";
		// 	$sql = sprintf($sql, $torneo_id, $ronda_id);
		// 	// echo $sql; die;
		// 	$equipos = $this->query($sql);
		// }
		// print_r($equipos);die;

		return $equipos;
	}

	public function getGanadores($torneo_id, $fase, $ronda_id = 1) {
		$this->virtualFields['count'] = 0;
		// $this->query("UPDATE equipos SET participando = 0 WHERE torneo_id = $torneo_id");
		$sql = "SELECT
				Equipo.*, Participante.*, COUNT(Respuesta.id) - Equipo.penalizacion as Equipo__count, Equipo.penalizacion
			FROM equipos as Equipo
			LEFT JOIN participantes as Participante ON Participante.id = Equipo.participante_id
			LEFT JOIN respuestas as Respuesta on Respuesta.equipo_id = Equipo.id AND Respuesta.es_correcta = 1 AND Respuesta.ronda_id = %s
			WHERE Equipo.torneo_id = %s AND Equipo.fase = %s AND Equipo.ronda_id = %s AND Equipo.participando = 1
			GROUP BY Equipo.id
			ORDER BY Equipo__count DESC";
		$sql = sprintf($sql, $ronda_id, $torneo_id, $fase, $ronda_id);
		// echo $sql; die;
		$equipos = $this->query($sql);
		$count_ganadores = 0;
		$ganadores = array();
		$ganador = isset($equipos['0']) ? $equipos['0'] : array();
		foreach ($equipos as $equipo) {
			if ($equipo['Equipo']['count'] == $ganador['Equipo']['count']) {
				$count_ganadores++;
				$equipo['Equipo']['participando'] = 1;
				$ganadores[] = $equipo;
			} else {
				$equipo['Equipo']['participando'] = 0;
			}
			$this->save($equipo);
		}
		// echo $count_ganadores;die;

		return $ganadores;
	}

}